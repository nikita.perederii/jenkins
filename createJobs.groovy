pipelineJob('pipelineJob') {
    definition {
        cps {
            script(readFileFromWorkspace('pipelineJob.groovy'))
            sandbox()
        }
    }
}
pipelineJob('test-branch') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url 'https://gitlab.com/nikita.perederii/demo.git'
                    }
                    branch 'master'
                }
            }
        }
    }
}